﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ASSN3
{
	public partial class WebViewShow : ContentPage
	{
		public WebViewShow(string WebUrls)
		{
			InitializeComponent();

            ShowWeb.Source = WebUrls;
            System.Diagnostics.Debug.WriteLine($"Watch for this line!!!!!!!!!!!{WebUrls}");
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}
