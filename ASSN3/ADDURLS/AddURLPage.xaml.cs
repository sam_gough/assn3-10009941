﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class AddURLPage : ContentPage
	{
        private SQLiteAsyncConnection _connection;
        
        public AddURLPage()
		{
			InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

		async void AddURL(object sender, EventArgs e)
		{
                var WebUrls = new WebURL { Title = TitleNew.Text, ImageUrl = ImageNew.Text, WebUrls = URLNew.Text };
                await _connection.InsertAsync(WebUrls);

                await DisplayAlert("Web List", "URL Added Successfully", "Ok");

                TitleNew.Text = "";
                ImageNew.Text = "";
                URLNew.Text = "";
                   
        }

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}

	}
}
