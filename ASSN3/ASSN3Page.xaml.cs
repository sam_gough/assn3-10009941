﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int UrlId { get; set; }

		public string WebUrls { get; set; }
		public string ImageUrl { get; set; }
		public string Title { get; set; }
	}

	public partial class ASSN3Page : ContentPage
	{

        private SQLiteAsyncConnection _connection;
        private List<WebURL> UrlList;

        public ASSN3Page()
		{
			InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        async void ReloadPicker()
		{

            await _connection.CreateTableAsync<WebURL>();
            UrlList = await _connection.Table<WebURL>().ToListAsync();

            if (ShowUrls.Items.Count != 0)
			{
				ShowUrls.Items.Clear();
			}

			foreach (var x in UrlList)
			{
				ShowUrls.Items.Add($"{x.Title}");
			}
		}

		protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
            if(ShowUrls.SelectedIndex == -1)
            {
                await DisplayAlert("WARNING!", "Please Select An Item", "OK");
            }
            else
            {
                var passOnUrl = UrlList[ShowUrls.SelectedIndex].WebUrls;

                await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
            }
            
		}

		async void AddUrlAction(object sender, System.EventArgs e)
		{
			await Navigation.PushModalAsync(new AddURLPage());
		}

		async void UpdateUrlAction(object sender, System.EventArgs e)
		{
            if (ShowUrls.SelectedIndex == -1)
            {
                await DisplayAlert("WARNING!", "Please Select An Item", "OK");
            }
            else
            {
                UrlList = await _connection.Table<WebURL>().ToListAsync();
                var selectedItems = ShowUrls.SelectedIndex;
                var selectedClass = UrlList[ShowUrls.SelectedIndex];
                await Navigation.PushModalAsync(new EditURLPage(selectedClass, selectedItems));
            }
                
		}

		async void RemoveUrl(object sender, System.EventArgs e)
		{
            if (ShowUrls.SelectedIndex == -1)
            {
                await DisplayAlert("WARNING!", "Please Select An Item", "OK");
            }
            else
            {
                UrlList = await _connection.Table<WebURL>().ToListAsync();
                var selectedItem = UrlList[ShowUrls.SelectedIndex];
                await _connection.DeleteAsync(selectedItem);
                await DisplayAlert("Confirmed", "Item has been deleted", "OK");
                ReloadPicker();
            }
                
        }
	}
}