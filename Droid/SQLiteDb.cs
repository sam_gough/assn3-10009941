﻿using System;
using System.IO;
using SQLite;
using Xamarin.Forms;
using SetupSQLite.Droid;

[assembly: Dependency(typeof(SQLiteDb))]

namespace SetupSQLite.Droid
{
	public class SQLiteDb : ISQLiteDb
	{
		public SQLiteAsyncConnection GetConnection()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var path = Path.Combine(documentsPath, "WebListDb10009941.db3");

			return new SQLiteAsyncConnection(path);
		}
	}
}

